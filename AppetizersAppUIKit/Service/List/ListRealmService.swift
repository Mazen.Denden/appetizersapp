//
//  ListRealmService.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 4/7/2022.
//

class ListRealmService: ListServiceProtocol {
    private let realmManager: RealmManagerQueryProtocol
    
    init(realmManager: RealmManagerQueryProtocol = RealmManager.shared) {
        self.realmManager = realmManager
        
        Log.info("Retrieving data from Realm")
    }
    
//    func appetizers() async throws -> [Appetizer] {
//        return try realmManager.fetchObjects(Appetizer.self) ?? []
//    }
    
    func appetizers(completion: @escaping (Result<[Appetizer], ApiError>) -> Void) {
        do {
            if let appetizers = try realmManager.fetchObjects(Appetizer.self) {
                completion(.success(appetizers))
            } else {
                completion(.failure(ApiError.invalidData))
            }
            
        } catch {
            completion(.failure(ApiError.unableToComplete))
        }
    }
}

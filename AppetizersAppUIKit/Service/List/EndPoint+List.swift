//
//  EndPoint+List.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 20/6/2022.
//

extension Endpoint {
    static var appetizers: Endpoint {
         return Endpoint(path: "/swiftui-fundamentals/appetizers")
    }
}

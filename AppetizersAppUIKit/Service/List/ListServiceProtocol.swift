//
//  ListServiceProtocol.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 21/6/2022.
//

protocol ListServiceProtocol {
//    func appetizers() async throws -> [Appetizer]
    func appetizers(completion: @escaping (Result<[Appetizer], ApiError>) -> Void)
}

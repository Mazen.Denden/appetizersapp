//
//  ListService.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 20/6/2022.
//

import Foundation

class ListService: ListServiceProtocol {
    private let networkService: NetworkManagerProtocol
    private let realmManager: RealmManagerAddProtocol = RealmManager.shared
    
    init(networkService: NetworkManagerProtocol = NetworkManager.shared) {
        self.networkService = networkService
        
        Log.info("Retrieving data from API")
    }
    
//    func appetizers() async throws -> [Appetizer] {
//        let endPoint = Endpoint.appetizers
//        let response = try await networkService.fetch(type: AppetizerResponse.self, from: endPoint)
//
//        saveToRealm(response.request)
//
//        return response.request
//    }
    
    func appetizers(completion: @escaping (Result<[Appetizer], ApiError>) -> Void) {
        let endPoint = Endpoint.appetizers
        networkService.fetch(type: AppetizerResponse.self, from: endPoint) { [weak self] result in
            switch result {
            case .success(let appetizerResponse):
                self?.saveToRealm(appetizerResponse.request)
                completion(.success(appetizerResponse.request))
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func saveToRealm(_ array: [Appetizer]) {
        DispatchQueue.main.async {
            do {
                try self.realmManager.add(array)
            } catch {
                print("Error \(error)")
            }
        }
    }
    
}

//
//  DataProvider.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 4/7/2022.
//

struct DataProvider {
    
    enum DataProvidedFrom {
        case api
        case realm
    }
    
    // Some kind of logic here to determine where to fetch data from
    var dataFrom: DataProvidedFrom = .realm
    var realmManager: RealmManagerQueryProtocol = RealmManager.shared
    
    init() {
        if let appetizers = try? realmManager.fetchObjects(Appetizer.self) {
            dataFrom = appetizers.isEmpty ? .api : .realm
        }
    }
    
    init(dataFrom: DataProvidedFrom) {
        self.dataFrom = dataFrom
    }
    
    func service() -> ListServiceProtocol {
        switch dataFrom {
        case .api: return ListService()
        case .realm: return ListRealmService()
        }
    }
    
}

//
//  AppetizerResponse.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 21/6/2022.
//

struct AppetizerResponse: Codable {
    let request: [Appetizer]
}

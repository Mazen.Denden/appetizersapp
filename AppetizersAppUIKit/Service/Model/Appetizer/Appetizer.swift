//
//  Appetizer.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 8/6/2022.
//

import Realm
import RealmSwift

class Appetizer: Object, Codable {
    @Persisted(primaryKey: true)
    var id: Int
    
    @Persisted
    var name: String
    
    @Persisted
    var price: Double
    
    @Persisted
    var imageURL: String
    
    @Persisted
    var appetizerDescription: String
    
    enum CodingKeys: String, CodingKey {
        case id, name, price, imageURL
        case appetizerDescription = "description"
    }
}

extension Appetizer {
    static let samples: [Appetizer] = [sampleAppetizer, sampleAppetizer, sampleAppetizer]
    static let appetizerDict: [String: Any] = ["id": 2,
                                               "name": "Blackened Shrimp",
                                               "price": 6.99,
                                               "imageURL": "https://seanallen-course-backend.herokuapp.com/images/appetizers/blackened-shrimp.jpg",
                                               "appetizerDescription": "Seasoned shrimp from the depths of the Atlantic Ocean."]
    static let sampleAppetizer = Appetizer(value: appetizerDict)
}

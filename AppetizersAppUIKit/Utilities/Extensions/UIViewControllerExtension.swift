//
//  UIViewControllerExtension.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 21/6/2022.
//

import UIKit

extension UIViewController {
    func showAlert(title: String, message: String, buttonTitle: String, action: @escaping (UIAlertAction) -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: action))
        self.present(alert, animated: true, completion: nil)
    }
}

extension UIViewController {
    static func instantiate<T: UIViewController>(from storyboardId: String) -> T {
        let storyboard = UIStoryboard(name: storyboardId, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: String(describing: T.self)) as! T
        return viewController
    }
}

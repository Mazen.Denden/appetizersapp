//
//  StoryboardIds.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 24/6/2022.
//

import Foundation

struct StoryboardId {
    static let list = "List"
    static let tabBar = "TabBar"
    static let details = "AppetizerDetails"
    static let favorites = "Favorites"
}

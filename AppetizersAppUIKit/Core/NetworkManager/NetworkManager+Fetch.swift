//
//  NetworkManager+Fetch.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 21/6/2022.
//

import Foundation

extension NetworkManager: NetworkManagerProtocol {
//    func fetch<T: Decodable>(type: T.Type, from endPoint: Endpoint) async throws -> T {
//        guard let url = endPoint.url else {
//            throw ApiError.invalidURL
//        }
//
//        let (data, response) = try await urlSession.data(from: url)
//
//        guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
//            throw ApiError.invalidResponse
//        }
//
//        do {
//            let decodedObject = try decoder.decode(T.self, from: data)
//            return decodedObject
//
//        } catch {
//            throw ApiError.failedToDecode
//        }
//    }
    
    func fetch<T: Decodable>(type: T.Type, from endPoint: Endpoint, completion: @escaping (Result<T, ApiError>) -> Void) {
        guard let url = endPoint.url else {
            completion(.failure(ApiError.invalidURL))
            return
        }

        let dataTask = urlSession.dataTask(with: url) { data, response, error in
            if error != nil {
                completion(.failure(ApiError.unableToComplete))
                return
            }

            if let response = response as? HTTPURLResponse, response.statusCode != 200 {
                completion(.failure(ApiError.invalidResponse))
                return
            }

            if let data = data {
                do {
                    let decodedData = try self.decoder.decode(T.self, from: data)
                    completion(.success(decodedData))
                } catch {
                    completion(.failure(ApiError.failedToDecode))
                }

            } else {
                completion(.failure(ApiError.invalidData))
                return
            }
        }

        dataTask.resume()
    }
}

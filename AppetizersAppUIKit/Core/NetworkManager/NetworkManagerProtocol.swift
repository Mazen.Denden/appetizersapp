//
//  NetworkManagerProtocol.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 21/6/2022.
//

protocol NetworkManagerProtocol {
//    func fetch<T: Decodable>(type: T.Type, from endPoint: Endpoint) async throws -> T
    func fetch<T: Decodable>(type: T.Type, from endPoint: Endpoint, completion: @escaping (Result<T, ApiError>) -> Void)
}

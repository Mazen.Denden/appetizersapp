//
//  EndPoint.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 17/6/2022.
//

import Foundation

struct Endpoint {
    let path: String
    var queryItems: [URLQueryItem] = []
}

extension Endpoint {
    var url: URL? {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "seanallen-course-backend.herokuapp.com"
        components.path = path
        components.queryItems = queryItems

        return components.url
    }
}

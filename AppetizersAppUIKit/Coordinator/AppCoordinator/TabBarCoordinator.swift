//
//  TabBarCoordinator.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 27/6/2022.
//

import UIKit

final class TabBarCoordinator: Coordinator {
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    var tabBarController: TabBarViewController?
    
    enum TabType {
        case home
        case favorites
    }
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.navigationController.setNavigationBarHidden(true, animated: false)
    }
    
    func start() {
        tabBarController = TabBarViewController.instantiate(from: StoryboardId.tabBar)
        
        [.home, .favorites].forEach { startTabFlow($0) }
        
        navigationController.pushViewController(tabBarController!, animated: false)
    }
    
    func startTabFlow(_ tab: TabType) {
        var coordinator: Coordinator?
        
        switch tab {
        case .home:
            coordinator = HomeCoordinator(navigationController: tabBarController!.viewControllers?[0] as! UINavigationController)
        case .favorites:
            coordinator = FavoritesCoordinator(navigationController: tabBarController!.viewControllers?[1] as! UINavigationController)
        }
        
        childCoordinators.append(coordinator!)
        coordinator!.start()
    }
    
}

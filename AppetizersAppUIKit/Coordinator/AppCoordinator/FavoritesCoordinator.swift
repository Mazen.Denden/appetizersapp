//
//  FavoritesCoordinator.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 27/6/2022.
//

import UIKit

final class FavoritesCoordinator: Coordinator {
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let viewController = navigationController.viewControllers.first as! FavoritesViewController
        viewController.coordinator = self
    }
}

extension FavoritesCoordinator: FavoritesViewControllerNavigation {
    func didMoveToDetails(with appetizerViewModel: AppetizerViewModel) {
        let viewController: AppetizerDetailsViewController = AppetizerDetailsViewController.instantiate(from: StoryboardId.details)
        viewController.viewModel = appetizerViewModel
        navigationController.pushViewController(viewController, animated: true)
    }
}

//
//  HomeCoordinator.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 27/6/2022.
//

import UIKit

final class HomeCoordinator: Coordinator {
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let viewController = navigationController.viewControllers.first as! ListViewController
        viewController.coordinator = self
    }
}

extension HomeCoordinator: ListViewControllerNavigation {
    func didMoveToDetails(with appetizerViewModel: AppetizerViewModel) {
        let viewController: AppetizerDetailsViewController = AppetizerDetailsViewController.instantiate(from: StoryboardId.details)
        viewController.viewModel = appetizerViewModel
        navigationController.pushViewController(viewController, animated: true)
    }
}

//
//  AppCoordinator.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 24/6/2022.
//

import UIKit

final class AppCoordinator: Coordinator {
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    private let window: UIWindow

    init(window: UIWindow) {
        self.window = window
        self.navigationController = UINavigationController()
    }
    
    func start() {
        let tabBarCoordinator = TabBarCoordinator(navigationController: navigationController)
        tabBarCoordinator.start()
        
        childCoordinators.append(tabBarCoordinator)
        
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
}

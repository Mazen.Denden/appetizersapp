//
//  CartViewController+TableView.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 16/6/2022.
//

import UIKit

extension FavoritesViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favoritesViewModel.favorites.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: AppetizerCell.reuseIdentifier,
                                                       for: indexPath) as? AppetizerCell else { return UITableViewCell() }
        cell.appetizerViewModel = favoritesViewModel.favorites[indexPath.row]
        cell.isFavoriteViewController = true
        cell.delegate = self
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        coordinator?.didMoveToDetails(with: favoritesViewModel.favorites[indexPath.row])
    }
    
}

extension FavoritesViewController: AppetizerCellDelegate {
    func removeAppetizer(for id: Int) {
        tableView.reloadData()
    }

}

//
//  FavoritesViewControllerNavigation.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 29/6/2022.
//

protocol FavoritesViewControllerNavigation: AnyObject {
    func didMoveToDetails(with appetizerViewModel: AppetizerViewModel)
}

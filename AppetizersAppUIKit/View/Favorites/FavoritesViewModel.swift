//
//  FavoritesViewModel.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 23/6/2022.
//

final class FavoritesViewModel {
    var favorites: [AppetizerViewModel] {
        return Favorites.appetizers.filter { $0.isFavorite }
    }
    
    let title: String = "Favorites"
    
    func unfavoriteAppetizer(with id: Int) {
        if let appetizer = favorites.first(where: { $0.id == id }) {
            appetizer.toggleFavorite(with: false)
        }
    }
    
}

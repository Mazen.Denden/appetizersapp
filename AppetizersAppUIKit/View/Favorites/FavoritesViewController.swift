//
//  CartViewController.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 16/6/2022.
//

import UIKit

class FavoritesViewController: UITableViewController {
    
    weak var coordinator: FavoritesViewControllerNavigation?
    
    let favoritesViewModel = FavoritesViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.reloadData()
    }
    
}

// MARK: - Setup Functions
extension FavoritesViewController {
    private func setupViews() {
        setupNavBar()
        setupTableView()
    }
    
    private func setupNavBar() {
        navigationItem.title = favoritesViewModel.title
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.tintColor = .white
        navigationItem.backButtonTitle = ""
    }
    
    private func setupTableView() {
        let appetizerCell = UINib(nibName: AppetizerCell.reuseIdentifier, bundle: nil)
        tableView.register(appetizerCell, forCellReuseIdentifier: AppetizerCell.reuseIdentifier)
    }
}

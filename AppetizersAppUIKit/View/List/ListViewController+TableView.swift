//
//  ListViewController+TableView.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 8/6/2022.
//

import UIKit

extension ListViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listViewModel?.numberOfRows ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: AppetizerCell.reuseIdentifier,
                                                       for: indexPath) as? AppetizerCell else { return UITableViewCell() }
        cell.appetizerViewModel = listViewModel?.appetizer(at: indexPath.row)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        coordinator?.didMoveToDetails(with: listViewModel?.appetizers[indexPath.row] ?? AppetizerViewModel(appetizer: Appetizer.sampleAppetizer))
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.setTemplateWithSubviews(listViewModel?.isLoading ?? false, animate: true, viewBackgroundColor: .systemBackground)
    }
    
}

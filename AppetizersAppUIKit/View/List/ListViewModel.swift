//
//  ListViewModel.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 8/6/2022.
//

import Foundation

final class ListViewModel {
    
    var appetizers: [AppetizerViewModel] = []
    var isLoading: Bool = true
    
    var onAppetizersReceived: (() -> Void)?
    var onError: ((ApiError) -> Void)?
    
    let numberOfShimmerCells: Int = 8
    let listService: ListServiceProtocol
    
    init(listService: ListServiceProtocol = ListService()) {
        self.listService = listService
    }
    
    var title: String {
        return "🍟 Appetizers"
    }
    
    var numberOfRows: Int {
        isLoading ? numberOfShimmerCells : appetizers.count
    }
    
    func appetizer(at index: Int) -> AppetizerViewModel? {
        return index < appetizers.count && index >= 0 ? appetizers[index] : nil
    }
    
}

extension ListViewModel {
    
//    func appetizers() async {
//        do {
//            appetizers = try await listService.appetizers().map { AppetizerViewModel(appetizer: $0) }
//            Favorites.appetizers = appetizers
//
//        } catch {
//            self.error = error as? ApiError
//        }
//
//        isLoading = false
//    }
    
    func fetchAppetizers() {
        listService.appetizers { [weak self] result in
            switch result {
            case .success(let appetizers):
                self?.appetizers = appetizers.map { AppetizerViewModel(appetizer: $0) }
                Favorites.appetizers = self?.appetizers ?? []
                
                self?.onAppetizersReceived?()

            case .failure(let error):
                self?.onError?(error)
            }
            
            self?.isLoading = false
        }
    }
    
}

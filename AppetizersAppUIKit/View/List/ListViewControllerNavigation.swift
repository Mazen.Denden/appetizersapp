//
//  ListViewControllerNavigation.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 29/6/2022.
//

protocol ListViewControllerNavigation: AnyObject {
    func didMoveToDetails(with appetizerViewModel: AppetizerViewModel)
}

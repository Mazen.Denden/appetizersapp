//
//  AppetizerCell+Shimmer.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 21/6/2022.
//

import UIKit
import UIView_Shimmer

extension AppetizerCell: ShimmeringViewProtocol {
    var shimmeringAnimatedItems: [UIView] {
        [
            appetizerImageView,
            nameLabel,
            priceLabel
        ]
    }
    
}

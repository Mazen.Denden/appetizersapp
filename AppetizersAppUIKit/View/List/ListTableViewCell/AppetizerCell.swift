//
//  AppetizerCell.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 8/6/2022.
//

import UIKit
import Kingfisher

protocol AppetizerCellDelegate: AnyObject {
    func removeAppetizer(for id: Int)
}

final class AppetizerCell: UITableViewCell {
    static let reuseIdentifier: String = "AppetizerCell"
    
    weak var delegate: AppetizerCellDelegate?
    
    var isFavoriteViewController: Bool = false
    
    @IBOutlet weak var appetizerImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    
    var appetizerViewModel: AppetizerViewModel? {
        didSet {
            guard let viewModel = appetizerViewModel else { return }
            setupViewModel(viewModel: viewModel)
            setupBindings(viewModel: viewModel)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        appetizerImageView.layer.cornerRadius = 10
        appetizerImageView.clipsToBounds = true
    }
    
    @IBAction func favoriteAction(_ sender: Any) {
        appetizerViewModel?.toggleFavorite()
        
        if isFavoriteViewController, let viewModel = appetizerViewModel {
            delegate?.removeAppetizer(for: viewModel.id)
        }
    }
    
}

// MARK: - Setup Functions
extension AppetizerCell {
    private func setupViewModel(viewModel: AppetizerViewModel) {
        nameLabel.text = viewModel.name
        priceLabel.text = viewModel.price
        setImage(for: viewModel.isFavorite)
        
        fetchImage(with: viewModel.imageURL)
    }
    
    private func setupBindings(viewModel: AppetizerViewModel) {
        viewModel.onFavorite = { [weak self] isFavorite in
            self?.setImage(for: isFavorite)
        }
    }
    
    private func setImage(for isFavorite: Bool) {
        favoriteButton.setImage(UIImage(systemName: isFavorite ? "star.fill": "star"), for: .normal)
    }
}

// MARK: - Fetch Image
extension AppetizerCell {
    private func fetchImage(with url: URL) {
        appetizerImageView
            .kf
            .setImage(with: url,
                      placeholder: UIImage(named: "food-placeholder")!,
                      options: [
                        .loadDiskFileSynchronously,
                        .cacheOriginalImage,
                        .transition(.fade(0.25))
                      ])
    }
}

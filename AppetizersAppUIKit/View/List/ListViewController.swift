//
//  ListViewController.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 8/6/2022.
//

import UIKit

final class ListViewController: UITableViewController {

    weak var coordinator: ListViewControllerNavigation?
    
    var dataProvider = DataProvider()
    var listViewModel: ListViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViewModel()
        setupNavBar()
        setupTableView()
        setupBinders()
        
        fetchAppetizers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    // Commit 1
    // Commit 2
    // Commit 3
    
}

// MARK: - Setup Functions
extension ListViewController {
    
    func setupViewModel() {
        listViewModel = ListViewModel(listService: dataProvider.service())
    }
    
    func setupNavBar() {
        navigationItem.title = listViewModel?.title ?? ""
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.tintColor = .white
        navigationItem.backButtonTitle = ""
    }
    
    func setupTableView() {
        let appetizerCell = UINib(nibName: AppetizerCell.reuseIdentifier, bundle: nil)
        tableView.register(appetizerCell, forCellReuseIdentifier: AppetizerCell.reuseIdentifier)
        
        switchTableViewUserInteraction(enabled: false)
    }
    
    func fetchAppetizers() {
//        Task {
//            await listViewModel?.appetizers()
//        }
        
        listViewModel?.fetchAppetizers()
    }
    
}

extension ListViewController {
    private func setupBinders() {
        if let viewModel = listViewModel {
            viewModel.onAppetizersReceived = { [weak self] in
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                    self?.switchTableViewUserInteraction(enabled: true)
                }
            }
            
            viewModel.onError = { [weak self] error in
                DispatchQueue.main.async {
                    self?.showAlert(title: "Failed to retreive Appetizers",
                                    message: error.description,
                                    buttonTitle: "Refresh") { _ in
                        self?.fetchAppetizers()
                    }
                }
            }
        }
    }
    
    func switchTableViewUserInteraction(enabled: Bool) {
        tableView.isUserInteractionEnabled = enabled
    }
}

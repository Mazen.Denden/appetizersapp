//
//  AppetizerViewModel.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 8/6/2022.
//

import Foundation

final class AppetizerViewModel {
    
    let appetizer: Appetizer
    private(set) var isFavorite: Bool = false
    
    var onFavorite: ((Bool) -> Void)?
    
    init(appetizer: Appetizer) {
        self.appetizer = appetizer
    }
    
    var id: Int { return Int(appetizer.id) }
    var name: String { return appetizer.name }
    var imageURL: URL { return URL(string: appetizer.imageURL)! }
    var price: String { return "$\(String(format:"%.2f", appetizer.price))" }
    var description: String { return appetizer.appetizerDescription }
    
    func toggleFavorite(with isFavorite: Bool? = nil) {
        if isFavorite == nil {
            self.isFavorite.toggle()
            
        } else {
            self.isFavorite = isFavorite ?? false
        }
        
        onFavorite?(self.isFavorite)
    }
    
}

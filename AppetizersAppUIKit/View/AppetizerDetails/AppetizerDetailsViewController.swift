//
//  AppetizerDetailsViewController.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 8/6/2022.
//

import UIKit

class AppetizerDetailsViewController: UIViewController {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var appetizerImageView: UIImageView!
    
    var viewModel: AppetizerViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if let viewModel = viewModel {
            setupViewModel(viewModel: viewModel)
            setupBindings(viewModel: viewModel)
        }
    }
    
    private func setupViews() {
        setupNavBar()
    }
    
    private func setupNavBar() {
        let addToFavoriteBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "star"),
                                                     style: .plain,
                                                     target: self,
                                                     action: #selector(handleAddToFavorite))
        addToFavoriteBarButtonItem.tintColor = .systemYellow
        self.navigationItem.rightBarButtonItem = addToFavoriteBarButtonItem
    }
    
    private func setupRightBarButtonIcon(isFavorite: Bool) {
        navigationItem.rightBarButtonItem?.image = UIImage(systemName: isFavorite ? "star.fill": "star")
    }
    
    @objc private func handleAddToFavorite(_ sender: Any) {
        viewModel?.toggleFavorite()
    }
    
    private func setupBindings(viewModel: AppetizerViewModel) {
        viewModel.onFavorite = { [weak self] isFavorite in
            self?.setupRightBarButtonIcon(isFavorite: isFavorite)
        }
    }
    
    private func setupViewModel(viewModel: AppetizerViewModel) {
        nameLabel.text = viewModel.name
        priceLabel.text = viewModel.price
        descriptionLabel.text = viewModel.description
        
        setupRightBarButtonIcon(isFavorite: viewModel.isFavorite)
        fetchImage(with: viewModel.imageURL)
    }

    private func fetchImage(with url: URL) {
        appetizerImageView
            .kf
            .setImage(with: url,
                      placeholder: UIImage(named: "food-placeholder")!,
                      options: [
                        .loadDiskFileSynchronously,
                        .cacheOriginalImage,
                        .transition(.fade(0.25))
                      ])
    }
}

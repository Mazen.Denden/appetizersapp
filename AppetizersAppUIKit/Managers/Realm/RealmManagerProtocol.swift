//
//  RealmManagerProtocol.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 5/7/2022.
//

import RealmSwift

protocol RealmManagerProtocol: RealmManagerAddProtocol, RealmManagerQueryProtocol, RealmManagerDeleteProtocol {}

protocol RealmManagerAddProtocol {
    func add<T: Object>(_ object: T, update: Bool) throws
    func add<T: Object>(_ objects: [T]) throws
}

protocol RealmManagerQueryProtocol {
    func fetchObject<T: Object>(_ type: T.Type, key: Any) throws -> T?
    func fetchObjects<T: Object>(_ type: T.Type) throws -> [T]?
}

protocol RealmManagerDeleteProtocol {
    func delete<T: Object>(_ type: T.Type, key: Any) throws
    func delete<T: Object>(_ objects: [T]) throws
    func clearAllData() throws
}

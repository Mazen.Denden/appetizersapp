//
//  RealmManager.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 4/7/2022.
//

import Realm
import RealmSwift

class RealmManager: RealmManagerProtocol {
    
    static let shared = RealmManager()
    private init() {}
    
    func add<T: Object>(_ object: T, update: Bool = true) throws {
        let realm = try Realm()
        realm.refresh()
        
        try realm.write {
            realm.add(object, update: update ? .modified : .error)
        }
    }
    
    func add<T: Object>(_ objects: [T]) throws {
        for object in objects {
            try add(object)
        }
    }
    
    func fetchObjects<T: Object>(_ type: T.Type) throws -> [T]? {
        let realm = try Realm()
        realm.refresh()
        
        return Array(realm.objects(T.self))
    }
    
    func fetchObject<T: Object>(_ type: T.Type, key: Any) throws -> T? {
        let realm = try Realm()
        realm.refresh()

        return realm.object(ofType: type,
                            forPrimaryKey: key)
    }
    
    func delete<T: Object>(_ type: T.Type, key: Any) throws {
        let realm = try Realm()
        realm.refresh()
        
        guard let object = try fetchObject(T.self, key: key) else { return }
        
        try realm.write {
            realm.delete(object)
        }
    }
    
    func delete<T: Object>(_ objects: [T]) throws {
        let realm = try Realm()
        realm.refresh()
        
        try realm.write {
            realm.delete(objects)
        }
    }
    
    func clearAllData() throws {
        let realm = try Realm()
        realm.refresh()
        
        try realm.write {
            realm.deleteAll()
        }
    }
    
}

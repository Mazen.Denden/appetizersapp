//
//  ListViewModelTests.swift
//  AppetizersAppUIKitTests
//
//  Created by Mazen Denden on 27/6/2022.
//

import XCTest
@testable import AppetizersAppUIKit

class ListViewModelTests: XCTestCase {
    
    override func tearDownWithError() throws {
        Favorites.appetizers = []
    }
    
    func test_init() {
        let sut = ListViewModel(listService: ListService())
        XCTAssertNotNil(sut.listService)
    }
    
    func test_init_withSpy() {
        XCTAssertNotNil(makeSUT().listService)
    }
    
    func test_init_appetizersArray_hasValues() {
        let sut = makeSUT()
        
        sut.fetchAppetizers()
        
        XCTAssert(sut.appetizers.count > 0)
        XCTAssert(Favorites.appetizers.count > 0)
    }
    
    func test_init_appetizersArray_throws_Error() {
        let sut = makeSUT(isSuccess: false)
        
        sut.fetchAppetizers()
        
        XCTAssert(sut.appetizers.isEmpty)
        XCTAssert(Favorites.appetizers.isEmpty)
    }
    
    func test_isLoading() {
        let sut = makeSUT()
        
        sut.fetchAppetizers()
        
        XCTAssertFalse(sut.isLoading)
    }
    
    func test_numberOfRows() {
        let sut = ListViewModel(listService: ListService())
        XCTAssertEqual(sut.numberOfRows, sut.numberOfShimmerCells)
        
        sut.isLoading = false
        XCTAssertNotEqual(sut.numberOfRows, sut.numberOfShimmerCells)
    }
    
    func test_fetchSingleAppetizer_withSmallAndBigIndex_from_Empty_Array() {
        let sut = ListViewModel(listService: ListService())
        XCTAssertNil(sut.appetizer(at: 1))
        XCTAssertNil(sut.appetizer(at: 99))
    }
    
    func test_fetchSingleAppetizer_from_notEmpty_Array() {
        let sut = makeSUT()
        
        sut.fetchAppetizers()
        
        XCTAssertNotNil(sut.appetizer(at: 0))
        XCTAssertNotNil(sut.appetizer(at: sut.numberOfRows - 1))
        
        XCTAssertNil(sut.appetizer(at: -1))
        XCTAssertNil(sut.appetizer(at: sut.numberOfRows))
    }
    
    private func makeSUT(isSuccess: Bool = true) -> ListViewModel {
        return ListViewModel(listService: ListViewModelSpy(isSuccess: isSuccess))
    }

}

struct ListViewModelSpy: ListServiceProtocol {
    let isSuccess: Bool
    
    func appetizers(completion: @escaping (Result<[Appetizer], ApiError>) -> Void) {
        if isSuccess {
            completion(.success(Appetizer.samples))
            return
        }
        
        completion(.failure(.invalidData))
    }

}

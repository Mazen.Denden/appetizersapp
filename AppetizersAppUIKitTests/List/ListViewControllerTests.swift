//
//  ListViewControllerTests.swift
//  AppetizersAppUIKitTests
//
//  Created by Mazen Denden on 28/6/2022.
//

import XCTest
@testable import AppetizersAppUIKit

class ListViewControllerTests: XCTestCase {
    
    func test_init_fromStoryboard() {
        XCTAssertNotNil(makeSUT())
    }
    
    func test_init_createsViewModel() {
        let sut = makeSUT()
        sut.setupViewModel()
        XCTAssertNotNil(sut.listViewModel)
    }
    
    func test_navBar() {
        let sut = makeSUT()
        sut.loadViewIfNeeded()
        XCTAssertEqual(sut.navigationItem.title, sut.listViewModel?.title ?? "")
    }
    
    func test_tableView_userInteraction_beforeApiCall() {
        let sut = makeSUT()
        XCTAssertFalse(sut.tableView.isUserInteractionEnabled)
    }
    
    func test_tableView_userInteraction_afterApiCall() {
        let sut = makeSUT()
        sut.switchTableViewUserInteraction(enabled: true)
        XCTAssertTrue(sut.tableView.isUserInteractionEnabled)
    }
    
//    func test_tableView_numberOfRows_beforeApi() {
//        let sut = makeSUT()
//        XCTAssertEqual(sut.tableView.numberOfRows(inSection: 0), 8)
//    }
    
//    func test_tableView_numberOfRows_afterApi() {
//        let sut = makeSUT()
//
//        sut.fetchAppetizers()
//
//        XCTAssertEqual(sut.tableView.numberOfRows(inSection: 0), sut.listViewModel.appetizers.count)
//    }
    
    private func makeSUT() -> ListViewController {
        let sut: ListViewController = ListViewController.instantiate(from: StoryboardId.list)
        return sut
    }
}

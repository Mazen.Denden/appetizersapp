//
//  DataProviderTests.swift
//  AppetizersAppUIKitTests
//
//  Created by Mazen Denden on 5/7/2022.
//

import XCTest
@testable import AppetizersAppUIKit

class DataProviderTests: XCTestCase {
    
    func test_init_withApi() {
        let sut = DataProvider(dataFrom: .api).dataFrom
        XCTAssertEqual(sut, .api)
    }
    
    func test_init_withRealm() {
        let sut = DataProvider(dataFrom: .realm).dataFrom
        XCTAssertEqual(sut, .realm)
    }
    
    func test_dataFromApi_returns_listService() {
        let sut = DataProvider(dataFrom: .api)
        let service = sut.service()
        XCTAssert(service is ListService)
    }
    
    func test_dataFromRealm_returns_listRealmService() {
        let sut = DataProvider(dataFrom: .realm)
        let service = sut.service()
        XCTAssert(service is ListRealmService)
    }
    
}

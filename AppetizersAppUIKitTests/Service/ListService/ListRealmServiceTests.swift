//
//  ListRealmServiceTests.swift
//  AppetizersAppUIKitTests
//
//  Created by Mazen Denden on 5/7/2022.
//

import XCTest
import RealmSwift
@testable import AppetizersAppUIKit

class ListRealmServiceTests: XCTestCase {
    func test_init() {
        let sut = ListRealmService(realmManager: ListRealmSpy())
        XCTAssertNotNil(sut)
    }
    
    func test_WS_throwsError() {
        let spy = ListRealmSpy(returnType: .failure)
        let sut = ListRealmService(realmManager: spy)
        
        sut.appetizers { result in
            switch result {
            case .success: break
            case .failure(let error): XCTAssertNotNil(error)
            }
        }
    }
    
    func test_WS_Success_returnArray() {
        let spy = ListRealmSpy(returnType: .success)
        let sut = ListRealmService(realmManager: spy)

        sut.appetizers { result in
            switch result {
            case .success(let appetizers): XCTAssertFalse(appetizers.isEmpty)
            case .failure: XCTFail("Not supposed to catch any Errors on a success case")
            }
        }
    }

    func test_WS_Success_returnEmptyArray() {
        let spy = ListRealmSpy(returnType: .empty)
        let sut = ListRealmService(realmManager: spy)

        sut.appetizers { result in
            switch result {
            case .success(let appetizers): XCTAssertTrue(appetizers.isEmpty)
            case .failure: XCTFail("Not supposed to catch any Errors on an empty case")
            }
        }
    }
    
}

class ListRealmSpy: RealmManagerQueryProtocol {

    var returnType: ReturnType = .success
    init(returnType: ReturnType = .success) {
        self.returnType = returnType
    }
    
    func fetchObjects<T: Object>(_ type: T.Type) throws -> [T]? {
        switch returnType {
        case .success: return Appetizer.samples as? [T]
        case .failure: throw ApiError.invalidData
        case .empty: return []
        }
    }
    
    func fetchObject<T: Object>(_ type: T.Type, key: Any) throws -> T? {
        return nil
    }
    
}

//
//  ListServiceTests.swift
//  AppetizersAppUIKitTests
//
//  Created by Mazen Denden on 28/6/2022.
//

import XCTest
@testable import AppetizersAppUIKit

class ListServiceTests: XCTestCase {
    
    func test_WS_throwsError() {
        let spy = ListServiceSpy(returnType: .failure)
        let sut = ListService(networkService: spy)
        
        sut.appetizers { result in
            switch result {
            case .success: XCTFail("Not supposed to success on a failure case")
            case .failure(let error): XCTAssertNotNil(error)
            }
        }
    }
    
    func test_WS_Success_returnArray() {
        let spy = ListServiceSpy(returnType: .success)
        let sut = ListService(networkService: spy)
        
        sut.appetizers { result in
            switch result {
            case .success(let appetizers): XCTAssertFalse(appetizers.isEmpty)
            case .failure: XCTFail("Not supposed to catch any Errors on a success case")
            }
        }
    }
    
    func test_WS_Success_returnEmptyArray() {
        let spy = ListServiceSpy(returnType: .empty)
        let sut = ListService(networkService: spy)
        
        sut.appetizers { result in
            switch result {
            case .success(let appetizers): XCTAssertTrue(appetizers.isEmpty)
            case .failure: XCTFail("Not supposed to catch any Errors on an empty case")
            }
        }
    }
    
}

class ListServiceSpy: NetworkManagerProtocol {
    
    var returnType: ReturnType = .success
    init(returnType: ReturnType = .success) {
        self.returnType = returnType
    }
    
    func fetch<T>(type: T.Type, from endPoint: Endpoint, completion: @escaping (Result<T, ApiError>) -> Void) where T : Decodable {
        switch returnType {
        case .success:
            completion(.success(AppetizerResponse(request: Appetizer.samples) as! T))
        case .failure:
            completion(.failure(.invalidResponse))
        case .empty:
            completion(.success(AppetizerResponse(request: []) as! T))
        }
    }
}

//
//  ListServiceEndPointTests.swift
//  AppetizersAppUIKitTests
//
//  Created by Mazen Denden on 28/6/2022.
//

import XCTest
@testable import AppetizersAppUIKit

class ListServiceEndPointTests: XCTestCase {
    func test_list_valid_endPoint() {
        let sut = Endpoint.appetizers
        
        XCTAssertEqual(sut.url,
                       URL(string: "https://seanallen-course-backend.herokuapp.com/swiftui-fundamentals/appetizers?"))
    }
    
}

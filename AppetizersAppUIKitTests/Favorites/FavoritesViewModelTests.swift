//
//  FavoritesViewModelTests.swift
//  AppetizersAppUIKitTests
//
//  Created by Mazen Denden on 27/6/2022.
//

import XCTest
@testable import AppetizersAppUIKit

class FavoritesViewModelTests: XCTestCase {
    override func tearDownWithError() throws {
        Favorites.appetizers = []
    }
    
    func test_favorites_empty() {
        let sut = FavoritesViewModel()
        XCTAssert(sut.favorites.isEmpty)
    }
    
    func test_title() {
        let sut = FavoritesViewModel()
        XCTAssertEqual(sut.title, "Favorites")
    }
    
    func test_favorites_onlyContainsFavorites() {
        let list = ListViewModel(listService: ListViewModelSpy(isSuccess: true))
        let sut = FavoritesViewModel()
        
        list.fetchAppetizers()
        
        XCTAssertFalse(list.appetizers.isEmpty)
        XCTAssertTrue(sut.favorites.isEmpty)
        
        list.appetizers.first!.toggleFavorite()
        
        XCTAssertFalse(sut.favorites.isEmpty)
    }
    
    func test_unfavorite_Appetizer() {
        let list = ListViewModel(listService: ListViewModelSpy(isSuccess: true))
        let sut = FavoritesViewModel()
        
        list.fetchAppetizers()
        list.appetizers.first!.toggleFavorite()
        
        XCTAssertFalse(sut.favorites.isEmpty)
        sut.unfavoriteAppetizer(with: 2)
        XCTAssertTrue(sut.favorites.isEmpty)
        
    }
    
}

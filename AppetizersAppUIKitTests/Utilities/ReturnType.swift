//
//  ReturnType.swift
//  AppetizersAppUIKitTests
//
//  Created by Mazen Denden on 5/7/2022.
//

enum ReturnType {
    case success
    case failure
    case empty
}

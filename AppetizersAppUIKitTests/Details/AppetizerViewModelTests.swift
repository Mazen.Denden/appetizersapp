//
//  AppetizerViewModelTests.swift
//  AppetizersAppUIKitTests
//
//  Created by Mazen Denden on 27/6/2022.
//

import XCTest
@testable import AppetizersAppUIKit

class AppetizerViewModelTests: XCTestCase {
    func test_init() {
        let sut = AppetizerViewModel(appetizer: Appetizer.sampleAppetizer)
        XCTAssertNotNil(sut.appetizer)
        XCTAssertFalse(sut.isFavorite)
        XCTAssertEqual(sut.name, "Blackened Shrimp")
        XCTAssertEqual(sut.imageURL, URL(string: "https://seanallen-course-backend.herokuapp.com/images/appetizers/blackened-shrimp.jpg")!)
        XCTAssertEqual(sut.price, "$6.99")
        XCTAssertEqual(sut.description, "Seasoned shrimp from the depths of the Atlantic Ocean.")
    }
    
}

/*
 static let appetizerDict: [String: Any] = ["id": 2,
                                            "name": "Blackened Shrimp",
                                            "price": 6.99,
                                            "imageURL": "https://seanallen-course-backend.herokuapp.com/images/appetizers/blackened-shrimp.jpg"]
 static let sampleAppetizer = Appetizer(value: appetizerDict)
 */

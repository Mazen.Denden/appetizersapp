//
//  NetworkManagerTests.swift
//  AppetizersAppUIKitTests
//
//  Created by Mazen Denden on 29/6/2022.
//

import XCTest
@testable import AppetizersAppUIKit

class NetworkManagerTests: XCTestCase {
    func test_init() {
        let sut = NetworkManager.shared
        XCTAssertNotNil(sut.urlSession)
        XCTAssertNotNil(sut.decoder)
    }
    
    func test_fetch() async throws {
        let sut = NetworkManager.shared
        let endPoint = Endpoint(path: "/swiftui-fundamentals/appetizers")
        
        guard let url = endPoint.url else {
            XCTFail()
            throw ApiError.invalidURL
        }
        
        let (data, response) = try await sut.urlSession.data(from: url)
        
        XCTAssertLessThan((response as? HTTPURLResponse)?.statusCode ?? 0, 400)
        XCTAssertNotNil(data)
    }
    
}
